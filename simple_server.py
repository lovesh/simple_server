import os
import tornado.ioloop
import tornado.web
from views import *

template_path=os.path.join(os.path.dirname(__file__), "templates")

application = tornado.web.Application(handlers = routes, template_path = template_path, debug=True)

if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()

