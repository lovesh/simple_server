import tornado.web
import tornado.escape
from models import *
from auth import require_basic_auth

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html")

@require_basic_auth
class UsersHandler(tornado.web.RequestHandler):
    def get(self, user_id, key, secret):
        
        dev = Developers.getDevelopers(criteria = {'api_key':key}, limit=1)
        if len(dev) > 0:
            if dev[0]['secret'] == secret:
                user = Users.getUsers(criteria = {'id':user_id}, limit=1)
                if len(user) > 0:
                    self.write(tornado.escape.json_encode(user[0]))
                else:
                    self.write("No user with the user_id %s"%user_id)
            else:
                self.write("Use a valid pair of api key and secret")
        else:
            self.write("Use a valid pair of api key and secret")


routes = [
    (r"/", IndexHandler),
    (r"/users/(\d+)", UsersHandler), 
]

__all__ = ['IndexHandler', 'UsersHandler', 'routes']
