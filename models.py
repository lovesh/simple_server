import tornado.database

db = tornado.database.Connection(host="localhost", database="resources",user="root", password="root")

class Resources(object):
    
    @classmethod
    def getResources(cls, fields = [], criteria = {}, limit = None, order = None):
        """
        """
        
        if fields == []:
            field_string = '*'
        else:
            field_string = ','.join(fields)
        
        criteria_string = ' where '
        for key, value in criteria.items():
            value = "'" + value + "'"
            criteria_string = criteria_string + key + '=' + value + ' and '
        
        criteria_string = criteria_string[:-5]                #Remove the last ` and `
        
        query = "select %s from %s"%(field_string, cls.__name__.lower())
        
        if criteria != {}:
            query = query + criteria_string
        
        if limit:
            query = query + ' limit %d '%limit
            
        if order:
            query = query + ' order by %s '%order
            
        query = query.strip()
        resources = db.query(query)
        
        return resources

class Users(Resources):
    
    @classmethod
    def getUsers(cls, fields = [], criteria = {}, limit = None, order = None):
        users = super(Users, cls).getResources(fields = fields, criteria = criteria, limit = limit, order = order)
        return users
    
class Developers(Resources):
    
    @classmethod
    def getDevelopers(cls, fields = [], criteria = {}, limit = None, order = None):
        developers = super(Developers, cls).getResources(fields = fields, criteria = criteria, limit = limit, order = order)
        return developers

__all__ = ['Users', 'Developers']
