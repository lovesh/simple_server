# MySQL Navigator Xport
# Database: resources
# root@localhost

# CREATE DATABASE resources;
# USE resources;

#
# Table structure for table 'developers'
#

# DROP TABLE IF EXISTS developers;
CREATE TABLE `developers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `devname` varchar(100) DEFAULT NULL,
  `api_key` char(32) DEFAULT NULL,
  `secret` char(128) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `api_key` (`api_key`),
  UNIQUE KEY `api_key_2` (`api_key`),
  UNIQUE KEY `devname` (`devname`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

#
# Dumping data for table 'developers'
#

INSERT INTO developers VALUES (1,'lovesh','lovesh','lovesh123','lovesh.bond@gmail.com');
INSERT INTO developers VALUES (2,'prateek','prateek','prateek456','prateek@aadhaarup.com');
INSERT INTO developers VALUES (3,'shailesh','shailesh','shailesh789','shailesh@aadhaarup.com');

#
# Table structure for table 'users'
#

# DROP TABLE IF EXISTS users;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `username_2` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

#
# Dumping data for table 'users'
#

INSERT INTO users VALUES (1,'lovesh','lovesh.bond@gmail.com');
INSERT INTO users VALUES (2,'prateek','prateek@aadhaarup.com');
INSERT INTO users VALUES (3,'shailesh','shailesh@aadhaarup.com');

